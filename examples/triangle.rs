use anyhow::{anyhow, Result};
use ash::vk;
use cgmath::{perspective, point3, vec3, Deg, Matrix4};
use log::{debug, warn};
use std::marker::PhantomData;
use std::mem;
use std::time::Instant;
use winit::event::{ElementState, Event, KeyEvent, WindowEvent};
use winit::keyboard::{Key, NamedKey};
use winit::window::Window;
use winit::{dpi::LogicalSize, event_loop::EventLoop, window::WindowBuilder};

use sulfur::vertex::Vertex;
use sulfur::{AllocationInfo, Sulfur};

trait AppState {}

macro_rules! impl_app_state {
    ($($state:ident$(<$($gt:tt),*>)?),+) => {
        $(
        impl$(<$($gt),*>)? AppState for $state$(<$($gt),*>)? {}
        )+
    };
}

struct Initialized;
struct RenderPassCreated;
struct DescriptorSetLayoutCreated;
struct PipelineLayoutCreated;
struct PipelineCreated;
struct CommandPoolCreated<PT> {
    _pool_type: PhantomData<PT>,
}
struct TransferCommandPool;
struct CommandPoolsCreated;
struct VertexShaderStagingBufferCreated;
struct VertexShaderBufferCopied;
struct UniformBufferCreated;
struct DescriptorPoolCreated;
struct DescriptorSetsCreated;
struct SwapchainFramebuffersCreated;
struct GraphicsCommandBuffersCreated;
struct SynchronizationPrimitivesCreated;
struct ReadyForDrawing;

impl_app_state!(
    Initialized,
    RenderPassCreated,
    DescriptorSetLayoutCreated,
    PipelineLayoutCreated,
    PipelineCreated,
    CommandPoolCreated<PT>,
    CommandPoolsCreated,
    VertexShaderStagingBufferCreated,
    VertexShaderBufferCopied,
    UniformBufferCreated,
    DescriptorPoolCreated,
    DescriptorSetsCreated,
    SwapchainFramebuffersCreated,
    GraphicsCommandBuffersCreated,
    SynchronizationPrimitivesCreated,
    ReadyForDrawing
);

struct Triangle<'a, S: AppState> {
    _state: PhantomData<S>,
    data: AppData<'a>,
}

struct PipelineData {
    pipeline_layout: Option<vk::PipelineLayout>,
    pipeline: Option<vk::Pipeline>,
    render_pass: Option<vk::RenderPass>,
}

struct CommandData {
    graphics_command_pool: Option<vk::CommandPool>,
    graphics_command_buffers: Vec<vk::CommandBuffer>,
    transfer_command_pool: Option<vk::CommandPool>,
}

struct SynchronizationData {
    image_available_semaphores: Vec<vk::Semaphore>,
    render_finished_semaphores: Vec<vk::Semaphore>,
    in_flight_fences: Vec<vk::Fence>,
}

struct BufferData {
    staging_buffer: Option<vk::Buffer>,
    staging_buffer_allocation_info: Option<AllocationInfo>,
    buffer: Option<vk::Buffer>,
    buffer_allocation_info: Option<AllocationInfo>,
    uniform_buffer: Option<vk::Buffer>,
    uniform_buffer_allocation_info: Option<AllocationInfo>,
    framebuffers: Vec<vk::Framebuffer>,
}

struct DescriptorData {
    descriptor_pool: Option<vk::DescriptorPool>,
    descriptor_set_layout: Option<vk::DescriptorSetLayout>,
    descriptor_sets: Vec<vk::DescriptorSet>,
}

struct AppData<'a> {
    sulfur: &'a mut Sulfur,
    window: &'a Window,
    start_time: Instant,
    max_frames_in_flight: usize,
    pipeline_data: PipelineData,
    command_data: CommandData,
    synchronization_data: SynchronizationData,
    buffer_data: BufferData,
    descriptor_data: DescriptorData,
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct UBO {
    model: Matrix4<f32>,
    view: Matrix4<f32>,
    proj: Matrix4<f32>,
}

impl<'a> Triangle<'a, Initialized> {
    fn new(window: &'a Window, sulfur: &'a mut Sulfur) -> Self {
        let pipeline_data = PipelineData {
            pipeline_layout: Default::default(),
            pipeline: Default::default(),
            render_pass: Default::default(),
        };

        let command_data = CommandData {
            graphics_command_pool: Default::default(),
            graphics_command_buffers: vec![],
            transfer_command_pool: Default::default(),
        };

        let buffer_data = BufferData {
            framebuffers: vec![],
            buffer: Default::default(),
            buffer_allocation_info: Default::default(),
            staging_buffer: Default::default(),
            staging_buffer_allocation_info: Default::default(),
            uniform_buffer: Default::default(),
            uniform_buffer_allocation_info: Default::default(),
        };

        let synchronization_data = SynchronizationData {
            image_available_semaphores: vec![],
            render_finished_semaphores: vec![],
            in_flight_fences: vec![],
        };

        let descriptor_data = DescriptorData {
            descriptor_set_layout: Default::default(),
            descriptor_pool: Default::default(),
            descriptor_sets: vec![],
        };

        let max_frames_in_flight = sulfur.swapchain_details.image_views.len();
        let app_data = AppData {
            sulfur,
            window,
            start_time: Instant::now(),
            max_frames_in_flight,
            pipeline_data,
            command_data,
            buffer_data,
            synchronization_data,
            descriptor_data,
        };

        Self {
            _state: PhantomData,
            data: app_data,
        }
    }

    fn create_render_pass(mut self) -> Triangle<'a, RenderPassCreated> {
        let color_attachment = vk::AttachmentDescription::default()
            .samples(vk::SampleCountFlags::TYPE_1)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::STORE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::PRESENT_SRC_KHR);

        let attachments = &[color_attachment];

        let color_attachment_ref = vk::AttachmentReference::default()
            .attachment(0)
            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL);

        let attachment_refs = &[color_attachment_ref];

        let subpass = vk::SubpassDescription::default()
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .color_attachments(attachment_refs);

        let subpasses = &[subpass];

        let subpass_dependency = vk::SubpassDependency::default()
            .src_subpass(vk::SUBPASS_EXTERNAL)
            .dst_subpass(0)
            .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(vk::AccessFlags::NONE)
            .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE);

        let dependencies = &[subpass_dependency];

        self.data.pipeline_data.render_pass = Some(
            self.data
                .sulfur
                .create_render_pass(attachments, subpasses, dependencies)
                .unwrap(),
        );

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, RenderPassCreated> {
    fn create_descriptor_set_layout(mut self) -> Triangle<'a, DescriptorSetLayoutCreated> {
        let layout_binding = vk::DescriptorSetLayoutBinding::default()
            .binding(0)
            .descriptor_count(1)
            .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
            .stage_flags(vk::ShaderStageFlags::VERTEX);

        let layout_bindings = &[layout_binding];

        let layout_create_info =
            vk::DescriptorSetLayoutCreateInfo::default().bindings(layout_bindings);

        let descriptor_set_layout = unsafe {
            self.data
                .sulfur
                .device
                .create_descriptor_set_layout(&layout_create_info, None)
                .unwrap()
        };

        self.data.descriptor_data.descriptor_set_layout = Some(descriptor_set_layout);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, DescriptorSetLayoutCreated> {
    fn create_pipeline_layout(mut self) -> Triangle<'a, PipelineLayoutCreated> {
        let set_layouts = &[self.data.descriptor_data.descriptor_set_layout.unwrap()];

        let pipeline_layout_create_info =
            vk::PipelineLayoutCreateInfo::default().set_layouts(set_layouts);

        let pipeline_layout = self
            .data
            .sulfur
            .create_pipeline_layout(&pipeline_layout_create_info)
            .unwrap();

        self.data.pipeline_data.pipeline_layout = Some(pipeline_layout);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, PipelineLayoutCreated> {
    //noinspection RsUnresolvedPath
    fn create_pipeline(mut self) -> Triangle<'a, PipelineCreated> {
        let vertex_binding_description = Vertex::get_binding_description(0);

        let vertex_binding_descriptions = &[vertex_binding_description];

        let position_attribute_description = Vertex::get_attribute_description(
            0,
            0,
            mem::offset_of!(Vertex, position),
            vk::Format::R32G32_SFLOAT,
        );

        let color_attribute_description = Vertex::get_attribute_description(
            0,
            1,
            mem::offset_of!(Vertex, color),
            vk::Format::R32G32B32_SFLOAT,
        );

        let vertex_attribute_descriptions =
            &[position_attribute_description, color_attribute_description];

        let vertex_input_state = vk::PipelineVertexInputStateCreateInfo::default()
            .vertex_binding_descriptions(vertex_binding_descriptions)
            .vertex_attribute_descriptions(vertex_attribute_descriptions);

        let input_assembly_state = vk::PipelineInputAssemblyStateCreateInfo::default()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
            .primitive_restart_enable(false);

        let rasterization_state = vk::PipelineRasterizationStateCreateInfo::default()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::COUNTER_CLOCKWISE)
            .depth_bias_enable(false);

        let multisample_state = vk::PipelineMultisampleStateCreateInfo::default()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlags::TYPE_1);

        let color_blend_attachment_state = vk::PipelineColorBlendAttachmentState::default()
            .color_write_mask(vk::ColorComponentFlags::RGBA)
            .blend_enable(false);

        let color_blend_attachments = &[color_blend_attachment_state];

        let color_blend_state = vk::PipelineColorBlendStateCreateInfo::default()
            .attachments(color_blend_attachments)
            .logic_op_enable(false);

        let dynamic_state = vk::PipelineDynamicStateCreateInfo::default()
            .dynamic_states(&[vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR]);

        let vertex_shader_path = std::path::Path::new("./examples/shaders/build/triangle_vert.spv")
            .to_str()
            .unwrap();

        let fragment_shader_path =
            std::path::Path::new("./examples/shaders/build/triangle_frag.spv")
                .to_str()
                .unwrap();

        let pipeline = self
            .data
            .sulfur
            .create_graphics_pipeline(
                vertex_shader_path,
                fragment_shader_path,
                &vertex_input_state,
                &input_assembly_state,
                &rasterization_state,
                &multisample_state,
                None,
                &color_blend_state,
                &dynamic_state,
                self.data.pipeline_data.pipeline_layout.unwrap(),
                self.data.pipeline_data.render_pass.unwrap(),
                0,
            )
            .unwrap();

        self.data.pipeline_data.pipeline = Some(pipeline);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, PipelineCreated> {
    fn create_transfer_command_pool(
        mut self,
    ) -> Triangle<'a, CommandPoolCreated<TransferCommandPool>> {
        let command_pool = self
            .data
            .sulfur
            .create_command_pool(
                self.data.sulfur.queue_families.transfer_family,
                vk::CommandPoolCreateFlags::TRANSIENT,
            )
            .unwrap();

        self.data.command_data.transfer_command_pool = Some(command_pool);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, CommandPoolCreated<TransferCommandPool>> {
    fn create_graphics_command_pool(mut self) -> Triangle<'a, CommandPoolsCreated> {
        let command_pool = self
            .data
            .sulfur
            .create_command_pool(
                self.data.sulfur.queue_families.graphics_family,
                vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
            )
            .unwrap();

        self.data.command_data.graphics_command_pool = Some(command_pool);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, GraphicsCommandBuffersCreated> {
    fn allocate_staging_buffer(
        mut self,
        vertices: &Vec<Vertex>,
        indices: &Vec<u16>,
    ) -> Triangle<'a, VertexShaderStagingBufferCreated> {
        debug!("Allocating staging buffer");

        let vertices_size = mem::size_of::<Vertex>() * vertices.len();

        let indices_size = mem::size_of::<u16>() * indices.len();

        let queue_families = &[
            self.data.sulfur.queue_families.graphics_family,
            self.data.sulfur.queue_families.transfer_family,
        ];

        let staging_buffer = unsafe {
            self.data.sulfur.create_buffer(
                (vertices_size + indices_size) as u64,
                vk::BufferUsageFlags::TRANSFER_SRC
                    | vk::BufferUsageFlags::INDEX_BUFFER
                    | vk::BufferUsageFlags::VERTEX_BUFFER,
                queue_families,
            )
        }
        .unwrap();

        let allocation_info = unsafe {
            self.data.sulfur.allocate_memory(
                &vec![staging_buffer],
                vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
            )
        }
        .unwrap();

        unsafe {
            self.data
                .sulfur
                .device
                .bind_buffer_memory(staging_buffer, allocation_info.memory, 0)
                .unwrap();
        }

        unsafe {
            self.data
                .sulfur
                .copy_data_to_memory(vertices, allocation_info.memory, 0)
                .unwrap();
        }

        let index_buffer_offset = (mem::size_of::<Vertex>() * vertices.len()) as u64;

        unsafe {
            self.data
                .sulfur
                .copy_data_to_memory(indices, allocation_info.memory, index_buffer_offset)
                .unwrap();
        }

        self.data.buffer_data.staging_buffer = Some(staging_buffer);

        self.data.buffer_data.staging_buffer_allocation_info = Some(allocation_info);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, VertexShaderStagingBufferCreated> {
    fn copy_staging_buffer(
        mut self,
        vertices: &Vec<Vertex>,
        indices: &Vec<u16>,
    ) -> Triangle<'a, VertexShaderBufferCopied> {
        debug!("Copying staging buffer to device local memory");

        let vertices_size = mem::size_of::<Vertex>() * vertices.len();

        let indices_size = mem::size_of::<u16>() * indices.len();

        let queue_families = &[
            self.data.sulfur.queue_families.graphics_family,
            self.data.sulfur.queue_families.transfer_family,
        ];

        let buffer = unsafe {
            self.data.sulfur.create_buffer(
                (vertices_size + indices_size) as u64,
                vk::BufferUsageFlags::TRANSFER_DST
                    | vk::BufferUsageFlags::VERTEX_BUFFER
                    | vk::BufferUsageFlags::INDEX_BUFFER,
                queue_families,
            )
        }
        .unwrap();

        let allocation_info = unsafe {
            self.data
                .sulfur
                .allocate_memory(&vec![buffer], vk::MemoryPropertyFlags::DEVICE_LOCAL)
        }
        .unwrap();

        unsafe {
            self.data
                .sulfur
                .device
                .bind_buffer_memory(buffer, allocation_info.memory, 0)
                .unwrap();
        }

        unsafe {
            self.data
                .sulfur
                .copy_buffer(
                    self.data.command_data.transfer_command_pool.unwrap(),
                    self.data.buffer_data.staging_buffer.unwrap(),
                    0,
                    buffer,
                    0,
                    (vertices_size + indices_size) as u64,
                )
                .unwrap();
        }

        self.data.buffer_data.buffer = Some(buffer);
        self.data.buffer_data.buffer_allocation_info = Some(allocation_info);

        unsafe {
            self.data.sulfur.device.destroy_buffer(
                *self.data.buffer_data.staging_buffer.as_ref().unwrap(),
                None,
            );
        }

        unsafe {
            self.data.sulfur.device.free_memory(
                self.data
                    .buffer_data
                    .staging_buffer_allocation_info
                    .as_ref()
                    .unwrap()
                    .memory,
                None,
            );
        }

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, VertexShaderBufferCopied> {
    fn create_uniform_buffer(mut self) -> Triangle<'a, UniformBufferCreated> {
        debug!("Creating uniform buffer");

        let buffer_size = mem::size_of::<UBO>() * self.data.max_frames_in_flight;

        let buffer = unsafe {
            self.data.sulfur.create_buffer(
                buffer_size as u64,
                vk::BufferUsageFlags::UNIFORM_BUFFER,
                &[self.data.sulfur.queue_families.graphics_family],
            )
        }
        .unwrap();

        let allocation_info = unsafe {
            self.data.sulfur.allocate_memory(
                &vec![buffer],
                vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
            )
        }
        .unwrap();

        unsafe {
            self.data
                .sulfur
                .device
                .bind_buffer_memory(buffer, allocation_info.memory, 0)
                .unwrap();
        }

        self.data.buffer_data.uniform_buffer = Some(buffer);
        self.data.buffer_data.uniform_buffer_allocation_info = Some(allocation_info);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, UniformBufferCreated> {
    fn create_descriptor_pool(mut self) -> Triangle<'a, DescriptorPoolCreated> {
        let pool_size = vk::DescriptorPoolSize::default()
            .descriptor_count(self.data.max_frames_in_flight as u32)
            .ty(vk::DescriptorType::UNIFORM_BUFFER);

        let pool_sizes = &[pool_size];

        let pool_create_info = vk::DescriptorPoolCreateInfo::default()
            .pool_sizes(pool_sizes)
            .max_sets(self.data.max_frames_in_flight as u32);

        let descriptor_pool = unsafe {
            self.data
                .sulfur
                .device
                .create_descriptor_pool(&pool_create_info, None)
        }
        .unwrap();

        self.data.descriptor_data.descriptor_pool = Some(descriptor_pool);

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, DescriptorPoolCreated> {
    fn create_descriptor_sets(mut self) -> Triangle<'a, DescriptorSetsCreated> {
        let set_layouts = [self.data.descriptor_data.descriptor_set_layout.unwrap()]
            .repeat(self.data.max_frames_in_flight);

        let allocate_info = vk::DescriptorSetAllocateInfo::default()
            .descriptor_pool(self.data.descriptor_data.descriptor_pool.unwrap())
            .set_layouts(&set_layouts);

        let descriptor_sets = unsafe {
            self.data
                .sulfur
                .device
                .allocate_descriptor_sets(&allocate_info)
        }
        .unwrap();

        let ubo_size = mem::size_of::<UBO>();

        for i in 0..self.data.max_frames_in_flight {
            let buffer_info = vk::DescriptorBufferInfo::default()
                .buffer(self.data.buffer_data.uniform_buffer.unwrap())
                .offset((i * ubo_size) as u64)
                .range(ubo_size as u64);

            let buffer_infos = &[buffer_info];

            let descriptor_write = vk::WriteDescriptorSet::default()
                .dst_set(descriptor_sets[i])
                .dst_binding(0)
                .dst_array_element(0)
                .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
                .descriptor_count(1)
                .buffer_info(buffer_infos);

            unsafe {
                self.data
                    .sulfur
                    .device
                    .update_descriptor_sets(&[descriptor_write], &[]);
            }
        }

        self.data.descriptor_data.descriptor_sets = descriptor_sets;

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, DescriptorSetsCreated> {
    fn create_swapchain_framebuffers(mut self) -> Triangle<'a, SwapchainFramebuffersCreated> {
        let framebuffers = self
            .data
            .sulfur
            .create_swapchain_framebuffers(self.data.pipeline_data.render_pass.unwrap());

        self.data.buffer_data.framebuffers = framebuffers;

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, CommandPoolsCreated> {
    fn create_graphics_command_buffers(mut self) -> Triangle<'a, GraphicsCommandBuffersCreated> {
        let graphics_command_buffers = self
            .data
            .sulfur
            .create_graphics_command_buffers(
                self.data.command_data.graphics_command_pool.unwrap(),
                self.data.max_frames_in_flight as u32,
                vk::CommandBufferLevel::PRIMARY,
            )
            .unwrap();

        self.data.command_data.graphics_command_buffers = graphics_command_buffers;

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, SwapchainFramebuffersCreated> {
    fn create_synchronization_primitives(
        mut self,
    ) -> Triangle<'a, SynchronizationPrimitivesCreated> {
        let semaphore_create_info = vk::SemaphoreCreateInfo::default();

        let fence_create_info =
            vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);

        let mut image_available_semaphores: Vec<vk::Semaphore> = vec![];
        let mut render_finished_semaphores: Vec<vk::Semaphore> = vec![];
        let mut in_flight_fences: Vec<vk::Fence> = vec![];

        for _ in 0..self.data.max_frames_in_flight {
            image_available_semaphores.push(unsafe {
                self.data
                    .sulfur
                    .device
                    .create_semaphore(&semaphore_create_info, None)
                    .unwrap()
            });

            render_finished_semaphores.push(unsafe {
                self.data
                    .sulfur
                    .device
                    .create_semaphore(&semaphore_create_info, None)
                    .unwrap()
            });

            in_flight_fences.push(unsafe {
                self.data
                    .sulfur
                    .device
                    .create_fence(&fence_create_info, None)
                    .unwrap()
            });
        }

        self.data.synchronization_data.image_available_semaphores = image_available_semaphores;
        self.data.synchronization_data.render_finished_semaphores = render_finished_semaphores;
        self.data.synchronization_data.in_flight_fences = in_flight_fences;

        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, SynchronizationPrimitivesCreated> {
    fn ready_for_drawing(self) -> Triangle<'a, ReadyForDrawing> {
        Triangle {
            _state: PhantomData,
            data: self.data,
        }
    }
}

impl<'a> Triangle<'a, ReadyForDrawing> {
    unsafe fn record_command_buffer(
        &self,
        image_index: u32,
        vertices: &Vec<Vertex>,
        indices: &Vec<u16>,
        command_buffer: vk::CommandBuffer,
    ) {
        let command_buffer_begin_info = vk::CommandBufferBeginInfo::default();

        self.data
            .sulfur
            .device
            .begin_command_buffer(command_buffer, &command_buffer_begin_info)
            .unwrap();

        let clear_value = vk::ClearValue {
            color: vk::ClearColorValue {
                float32: [0.01, 0.01, 0.01, 0.0],
            },
        };

        let clear_values = &[clear_value];

        let rendering_info = vk::RenderPassBeginInfo::default()
            .render_pass(self.data.pipeline_data.render_pass.unwrap())
            .framebuffer(self.data.buffer_data.framebuffers[image_index as usize])
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: self.data.sulfur.swapchain_details.extent,
            })
            .clear_values(clear_values);

        self.data.sulfur.device.cmd_begin_render_pass(
            command_buffer,
            &rendering_info,
            vk::SubpassContents::INLINE,
        );

        self.data.sulfur.device.cmd_bind_pipeline(
            command_buffer,
            vk::PipelineBindPoint::GRAPHICS,
            self.data.pipeline_data.pipeline.unwrap(),
        );

        let vertex_buffers = [self.data.buffer_data.buffer.unwrap()];

        self.data.sulfur.device.cmd_bind_vertex_buffers(
            command_buffer,
            0,
            &vertex_buffers,
            &[0u64],
        );

        let index_buffer_offset = (mem::size_of::<Vertex>() * vertices.len()) as u64;

        self.data.sulfur.device.cmd_bind_index_buffer(
            command_buffer,
            self.data.buffer_data.buffer.unwrap(),
            index_buffer_offset,
            vk::IndexType::UINT16,
        );

        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: self.data.sulfur.swapchain_details.extent.width as f32,
            height: self.data.sulfur.swapchain_details.extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let viewports = &[viewport];

        self.data
            .sulfur
            .device
            .cmd_set_viewport(command_buffer, 0, viewports);

        let scissor = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: self.data.sulfur.swapchain_details.extent,
        };

        let scissors = &[scissor];

        self.data
            .sulfur
            .device
            .cmd_set_scissor(command_buffer, 0, scissors);

        self.data.sulfur.device.cmd_bind_descriptor_sets(
            command_buffer,
            vk::PipelineBindPoint::GRAPHICS,
            self.data.pipeline_data.pipeline_layout.unwrap(),
            0,
            &[self.data.descriptor_data.descriptor_sets[image_index as usize]],
            &[],
        );

        self.data
            .sulfur
            .device
            .cmd_draw_indexed(command_buffer, indices.len() as u32, 1, 0, 0, 0);

        self.data.sulfur.device.cmd_end_render_pass(command_buffer);

        self.data
            .sulfur
            .device
            .end_command_buffer(command_buffer)
            .unwrap();
    }

    unsafe fn update_uniform_buffer(&self, current_frame: usize) -> Result<()> {
        let elapsed = self.data.start_time.elapsed().as_secs_f32();

        let model = Matrix4::from_axis_angle(vec3(0.0f32, 0.0, 1.0), Deg(90.0) * elapsed);

        let view = Matrix4::look_at_rh(
            point3(0.0f32, 2.0, 2.0),
            point3(0.0, 0.0, 0.0),
            vec3(0.0, 0.0, 1.0),
        );

        let mut proj = perspective(
            Deg(45.0f32),
            self.data.sulfur.swapchain_details.extent.width as f32
                / self.data.sulfur.swapchain_details.extent.height as f32,
            0.1,
            10.0,
        );

        // Flip Y coordinate
        proj[1][1] *= -1.0;

        let ubo = UBO { model, view, proj };

        let ubo_size = mem::size_of::<UBO>();

        let memory_offset: u64 = current_frame as u64
            * (ubo_size as u64
                + Sulfur::alignment_padding(
                    ubo_size,
                    self.data
                        .buffer_data
                        .uniform_buffer_allocation_info
                        .as_ref()
                        .unwrap()
                        .alignment,
                ));

        self.data.sulfur.copy_data_to_memory(
            &vec![ubo],
            self.data
                .buffer_data
                .uniform_buffer_allocation_info
                .as_ref()
                .unwrap()
                .memory,
            memory_offset,
        )?;

        Ok(())
    }

    unsafe fn draw_frame(
        &mut self,
        current_frame: usize,
        vertices: &Vec<Vertex>,
        indices: &Vec<u16>,
    ) -> Result<()> {
        self.data.sulfur.device.wait_for_fences(
            &[self.data.synchronization_data.in_flight_fences[current_frame]],
            true,
            u64::MAX,
        )?;

        self.data
            .sulfur
            .device
            .reset_fences(&[self.data.synchronization_data.in_flight_fences[current_frame]])?;

        let (image_index, framebuffers) = self
            .data
            .sulfur
            .acquire_next_image(
                self.data.synchronization_data.image_available_semaphores[current_frame],
                vk::Fence::null(),
                self.data.window,
                &self.data.buffer_data.framebuffers,
                self.data.pipeline_data.render_pass.unwrap(),
            )
            .unwrap();

        if image_index.is_none() {
            if framebuffers.is_some() {
                self.data.buffer_data.framebuffers = framebuffers.unwrap();
            }

            return Ok(());
        }

        let image_index = image_index.unwrap();

        self.update_uniform_buffer(current_frame)?;

        self.data
            .sulfur
            .device
            .reset_command_buffer(
                self.data.command_data.graphics_command_buffers[current_frame],
                vk::CommandBufferResetFlags::empty(),
            )
            .unwrap();

        self.record_command_buffer(
            image_index,
            vertices,
            indices,
            self.data.command_data.graphics_command_buffers[current_frame],
        );

        let wait_semaphores =
            &[self.data.synchronization_data.image_available_semaphores[current_frame]];

        let command_buffers = &[self.data.command_data.graphics_command_buffers[current_frame]];

        let signal_semaphores =
            &[self.data.synchronization_data.render_finished_semaphores[current_frame]];

        let submit_info = vk::SubmitInfo::default()
            .wait_semaphores(wait_semaphores)
            .wait_dst_stage_mask(&[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
            .command_buffers(command_buffers)
            .signal_semaphores(signal_semaphores);

        self.data.sulfur.device.queue_submit(
            self.data.sulfur.graphics_queue,
            &[submit_info],
            self.data.synchronization_data.in_flight_fences[current_frame],
        )?;

        let swapchains = &[self.data.sulfur.swapchain];

        let image_indices = &[image_index];

        let present_info = vk::PresentInfoKHR::default()
            .wait_semaphores(signal_semaphores)
            .swapchains(swapchains)
            .image_indices(image_indices);

        match self
            .data
            .sulfur
            .swapchain_loader
            .queue_present(self.data.sulfur.present_queue, &present_info)
        {
            Ok(true) | Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                warn!("The swapchain is either suboptimal or out of date");

                self.data.buffer_data.framebuffers = self.data.sulfur.recreate_swapchain(
                    self.data.window,
                    &self.data.buffer_data.framebuffers,
                    self.data.pipeline_data.render_pass.unwrap(),
                )?;

                return Ok(());
            }
            Ok(false) => Ok(()),
            Err(e) => Err(anyhow!("Failed to present image: {}", e)),
        }
    }

    fn destroy(&self) {
        unsafe {
            for i in 0..self.data.max_frames_in_flight {
                self.data.sulfur.device.destroy_semaphore(
                    self.data.synchronization_data.image_available_semaphores[i],
                    None,
                );

                self.data.sulfur.device.destroy_semaphore(
                    self.data.synchronization_data.render_finished_semaphores[i],
                    None,
                );

                self.data
                    .sulfur
                    .device
                    .destroy_fence(self.data.synchronization_data.in_flight_fences[i], None);
            }

            self.data
                .sulfur
                .device
                .destroy_command_pool(self.data.command_data.transfer_command_pool.unwrap(), None);

            self.data
                .sulfur
                .device
                .destroy_command_pool(self.data.command_data.graphics_command_pool.unwrap(), None);

            self.data
                .buffer_data
                .framebuffers
                .iter()
                .for_each(|fb| self.data.sulfur.device.destroy_framebuffer(*fb, None));
            self.data
                .sulfur
                .device
                .destroy_pipeline(self.data.pipeline_data.pipeline.unwrap(), None);

            self.data
                .sulfur
                .device
                .destroy_pipeline_layout(self.data.pipeline_data.pipeline_layout.unwrap(), None);

            self.data
                .sulfur
                .device
                .destroy_render_pass(self.data.pipeline_data.render_pass.unwrap(), None);

            self.data
                .sulfur
                .device
                .destroy_buffer(self.data.buffer_data.uniform_buffer.unwrap(), None);

            self.data.sulfur.device.free_memory(
                self.data
                    .buffer_data
                    .uniform_buffer_allocation_info
                    .as_ref()
                    .unwrap()
                    .memory,
                None,
            );

            self.data
                .sulfur
                .device
                .destroy_descriptor_pool(self.data.descriptor_data.descriptor_pool.unwrap(), None);

            self.data.sulfur.device.destroy_descriptor_set_layout(
                self.data.descriptor_data.descriptor_set_layout.unwrap(),
                None,
            );

            self.data
                .sulfur
                .device
                .destroy_buffer(self.data.buffer_data.buffer.unwrap(), None);

            self.data.sulfur.device.free_memory(
                self.data
                    .buffer_data
                    .buffer_allocation_info
                    .as_ref()
                    .unwrap()
                    .memory,
                None,
            );
        }
    }
}

fn main() -> Result<()> {
    pretty_env_logger::init();

    let event_loop = EventLoop::new().unwrap();

    let window = WindowBuilder::new()
        .with_title("Triangle example")
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)
        .unwrap();

    let mut sulfur = Sulfur::new(&window)?;

    let vertices = vec![
        Vertex {
            position: [-0.5, -0.5],
            color: [1.0, 0.0, 0.0],
        },
        Vertex {
            position: [0.5, -0.5],
            color: [0.0, 1.0, 0.0],
        },
        Vertex {
            position: [0.5, 0.5],
            color: [0.0, 0.0, 1.0],
        },
        Vertex {
            position: [-0.5, 0.5],
            color: [1.0, 1.0, 1.0],
        },
    ];

    let indices = vec![0u16, 1, 2, 2, 3, 0];

    let mut triangle = Triangle::new(&window, &mut sulfur)
        .create_render_pass()
        .create_descriptor_set_layout()
        .create_pipeline_layout()
        .create_pipeline()
        .create_transfer_command_pool()
        .create_graphics_command_pool()
        .create_graphics_command_buffers()
        .allocate_staging_buffer(&vertices, &indices)
        .copy_staging_buffer(&vertices, &indices)
        .create_uniform_buffer()
        .create_descriptor_pool()
        .create_descriptor_sets()
        .create_swapchain_framebuffers()
        .create_synchronization_primitives()
        .ready_for_drawing();

    let mut current_frame: usize = 0;

    event_loop
        .run(move |event, elwt| {
            match event {
                // Request a redraw when all events were processed.
                Event::AboutToWait => triangle.data.window.request_redraw(),
                Event::WindowEvent { event, .. } => match event {
                    // Render a frame if our Vulkan app is not being destroyed.
                    WindowEvent::RedrawRequested if !elwt.exiting() => unsafe {
                        triangle
                            .draw_frame(current_frame, &vertices, &indices)
                            .unwrap();
                        current_frame = (current_frame + 1) % triangle.data.max_frames_in_flight;
                    },
                    // Destroy our Vulkan app.
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                state: ElementState::Pressed,
                                logical_key: Key::Named(NamedKey::Escape),
                                ..
                            },
                        ..
                    } => {
                        unsafe {
                            triangle.data.sulfur.device.device_wait_idle().unwrap();
                        }
                        triangle.destroy();
                        elwt.exit();
                    }
                    _ => {}
                },
                _ => {}
            }
        })
        .map_err(|e| anyhow!("{}", e))
}
