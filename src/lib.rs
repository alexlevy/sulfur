use std::io::Cursor;
use std::{
    collections::{BTreeMap, HashSet},
    ffi::CStr,
    mem,
};

use anyhow::{anyhow, Result};
use ash::ext::debug_utils;
use ash::khr::{surface, swapchain};
use ash::util::{read_spv, Align};
use ash::{vk, Device, Entry, Instance};
use log::*;
use winit::dpi::PhysicalSize;
use winit::raw_window_handle::{HasDisplayHandle, HasWindowHandle};
use winit::window::Window;

pub mod vertex;

pub struct Sulfur {
    pub instance: Instance,
    debug_utils_loader: debug_utils::Instance,
    messenger: Option<vk::DebugUtilsMessengerEXT>,
    pub physical_device: vk::PhysicalDevice,
    pub device: Device,
    pub queue_families: QueueFamilies,
    pub graphics_queue: vk::Queue,
    pub present_queue: vk::Queue,
    pub transfer_queue: vk::Queue,
    surface_loader: surface::Instance,
    surface: vk::SurfaceKHR,
    surface_details: SurfaceDetails,
    pub swapchain_loader: swapchain::Device,
    pub swapchain: vk::SwapchainKHR,
    pub swapchain_details: SwapchainDetails,
    pub images: Vec<vk::Image>,
}

pub struct AllocationInfo {
    pub memory: vk::DeviceMemory,
    pub alignment: vk::DeviceSize,
    pub size: vk::DeviceSize,
}

pub struct QueueFamilies {
    pub graphics_family: u32,
    pub present_family: u32,
    pub transfer_family: u32,
}

struct SurfaceDetails {
    capabilities: vk::SurfaceCapabilitiesKHR,
    formats: Vec<vk::SurfaceFormatKHR>,
    present_modes: Vec<vk::PresentModeKHR>,
}

pub struct SwapchainDetails {
    pub format: vk::SurfaceFormatKHR,
    pub extent: vk::Extent2D,
    pub image_views: Vec<vk::ImageView>,
}

impl Sulfur {
    pub fn new(window: &Window) -> Result<Self> {
        let entry = Entry::linked();

        let instance = unsafe { Self::create_instance(&entry, window)? };

        let debug_utils_loader = debug_utils::Instance::new(&entry, &instance);

        let messenger = if cfg!(debug_assertions) {
            unsafe { Some(Self::setup_debug_messenger(&debug_utils_loader)?) }
        } else {
            None
        };

        let required_extensions = vec![vk::KHR_SWAPCHAIN_NAME];

        let surface_loader = surface::Instance::new(&entry, &instance);

        let surface = unsafe {
            ash_window::create_surface(
                &entry,
                &instance,
                window.display_handle()?.as_raw(),
                window.window_handle()?.as_raw(),
                None,
            )?
        };

        let physical_device = unsafe {
            Self::pick_physical_device(&instance, &required_extensions, &surface_loader, surface)?
        };

        // Call to unwrap() is safe as pick_physical_device would fail if no graphics family queue was found on any device
        let graphics_family =
            unsafe { Self::find_graphics_queue_family(&instance, physical_device).unwrap() };

        debug!("Graphics queue family: {}", graphics_family);

        let present_family = unsafe {
            Self::find_present_queue_family(&instance, physical_device, &surface_loader, surface)
                .unwrap()
        };

        debug!("Present queue family: {}", present_family);

        let transfer_family = unsafe {
            match Self::find_transfer_queue_family(&instance, physical_device) {
                None => {
                    warn!("No dedicated transfer queue family found. Falling back to graphics queue family");
                    graphics_family
                }
                Some(i) => i,
            }
        };

        debug!("Transfer queue family: {}", transfer_family);

        let queue_families = QueueFamilies {
            graphics_family,
            present_family,
            transfer_family,
        };

        let device = unsafe {
            Self::create_logical_device(
                &instance,
                physical_device,
                &queue_families,
                &required_extensions,
            )?
        };

        let graphics_queue = unsafe { device.get_device_queue(queue_families.graphics_family, 0) };

        let present_queue = unsafe { device.get_device_queue(queue_families.present_family, 0) };

        let transfer_queue = unsafe { device.get_device_queue(queue_families.transfer_family, 0) };

        let surface_details =
            unsafe { Self::get_surface_details(&surface_loader, physical_device, surface) }?;

        let swapchain_loader = swapchain::Device::new(&instance, &device);

        let format = Self::choose_surface_format(&surface_details.formats)?;

        let extent = Self::choose_swap_extent(&surface_details, window.inner_size());

        let swapchain = unsafe {
            Self::create_swapchain(
                &swapchain_loader,
                &surface_details,
                surface,
                format,
                extent,
                &queue_families,
            )?
        };

        let images = unsafe { swapchain_loader.get_swapchain_images(swapchain)? };

        let image_views = unsafe { Self::create_image_views(&images, &format, &device) }?;

        let swapchain_details = SwapchainDetails {
            format,
            extent,
            image_views,
        };

        Ok(Self {
            instance,
            debug_utils_loader,
            messenger,
            physical_device,
            device,
            queue_families,
            graphics_queue,
            present_queue,
            transfer_queue,
            surface_loader,
            surface,
            surface_details,
            swapchain_loader,
            swapchain,
            swapchain_details,
            images,
        })
    }

    unsafe fn create_instance(entry: &Entry, window: &Window) -> Result<Instance> {
        let app_name = unsafe { CStr::from_bytes_with_nul_unchecked(b"Sulfur\0") };

        let app_info = vk::ApplicationInfo::default()
            .application_name(app_name)
            .application_version(vk::make_api_version(0, 0, 1, 0))
            .api_version(vk::make_api_version(0, 1, 0, 0))
            .engine_name(app_name)
            .engine_version(vk::make_api_version(0, 0, 1, 0));

        let layer_names = Self::instance_layers(entry)?;

        let extension_names = Self::instance_extensions(window)?;

        let mut messenger_create_info = Self::messenger_create_info();

        let mut create_info = vk::InstanceCreateInfo::default()
            .flags(Self::instance_flags())
            .enabled_layer_names(&layer_names)
            .enabled_extension_names(&extension_names)
            .application_info(&app_info);

        if cfg!(debug_assertions) {
            create_info = create_info.push_next(&mut messenger_create_info);
        }

        entry
            .create_instance(&create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    fn instance_flags() -> vk::InstanceCreateFlags {
        if cfg!(any(target_os = "macos", target_os = "ios")) {
            vk::InstanceCreateFlags::ENUMERATE_PORTABILITY_KHR
        } else {
            vk::InstanceCreateFlags::default()
        }
    }

    unsafe fn instance_layers(entry: &Entry) -> Result<Vec<*const i8>> {
        let validation_layer =
            CStr::from_bytes_with_nul_unchecked(b"VK_LAYER_KHRONOS_validation\0");

        let available_layers = entry
            .enumerate_instance_layer_properties()?
            .iter()
            .map(|p| p.layer_name)
            .map(|l| std::str::from_utf8_unchecked(mem::transmute(&l as &[i8])))
            .collect::<HashSet<_>>();

        let validation_layer_found = available_layers
            .iter()
            .find(|&&l| l == validation_layer.to_str().unwrap())
            .is_some();

        if cfg!(debug_assertions) && validation_layer_found {
            return Err(anyhow!("Validation layer requested but not supported"));
        }

        let mut layers: Vec<&CStr> = vec![];

        if cfg!(debug_assertions) {
            layers.push(validation_layer)
        };

        return Ok(layers
            .iter()
            .map(|l| l.as_ptr())
            .collect::<Vec<*const i8>>());
    }

    unsafe fn instance_extensions(window: &Window) -> Result<Vec<*const i8>> {
        let mut extensions =
            ash_window::enumerate_required_extensions(window.display_handle()?.as_raw())
                .unwrap()
                .to_vec();

        if cfg!(any(target_os = "macos", target_os = "ios")) {
            extensions.push(ash::khr::portability_enumeration::NAME.as_ptr());
            extensions.push(ash::khr::get_physical_device_properties2::NAME.as_ptr());
        }

        if cfg!(debug_assertions) {
            extensions.push(debug_utils::NAME.as_ptr());
        }

        Ok(extensions)
    }

    fn messenger_create_info<'a>() -> vk::DebugUtilsMessengerCreateInfoEXT<'a> {
        vk::DebugUtilsMessengerCreateInfoEXT::default()
            .message_severity(
                vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                    | vk::DebugUtilsMessageSeverityFlagsEXT::INFO
                    | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                    | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
            )
            .message_type(
                vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                    | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                    | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
            )
            .pfn_user_callback(Some(debug_callback))
    }

    unsafe fn setup_debug_messenger(
        debug_utils_loader: &debug_utils::Instance,
    ) -> Result<vk::DebugUtilsMessengerEXT> {
        let debug_info = Self::messenger_create_info();

        debug_utils_loader
            .create_debug_utils_messenger(&debug_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    unsafe fn check_physical_device_extensions_support(
        instance: &Instance,
        device: vk::PhysicalDevice,
        required_extensions: &Vec<&CStr>,
    ) -> Result<bool> {
        let extensions_properties = instance.enumerate_device_extension_properties(device)?;

        Ok(required_extensions
            .iter()
            .map(|&ext| ext.to_string_lossy())
            .all(|ext| {
                extensions_properties
                    .iter()
                    .any(|e| CStr::from_ptr(e.extension_name.as_ptr()).to_string_lossy() == ext)
            }))
    }

    unsafe fn rate_device_suitability(
        instance: &Instance,
        device: vk::PhysicalDevice,
        required_extensions: &Vec<&CStr>,
        surface_loader: &surface::Instance,
        surface: vk::SurfaceKHR,
    ) -> u32 {
        let mut score: u32 = 0;

        let graphics_family = Self::find_graphics_queue_family(instance, device);

        let device_properties = instance.get_physical_device_properties(device);

        let device_features = instance.get_physical_device_features(device);

        let device_name = CStr::from_ptr(device_properties.device_name.as_ptr()).to_string_lossy();

        debug!(
            "Checking suitability of physical device {} of type {:?}",
            device_name, device_properties.device_type
        );

        if graphics_family.is_none() {
            warn!("No graphics queue family found for device {}", device_name);
            return 0;
        }

        if device_features.geometry_shader != vk::TRUE {
            warn!("Device {:?} does not support geometry shader", device_name);
            return 0;
        }

        if !Self::check_physical_device_extensions_support(instance, device, &required_extensions)
            .unwrap()
        {
            let required_extensions_names = &required_extensions
                .iter()
                .map(|&ext| ext.to_string_lossy())
                .collect::<Vec<_>>();

            warn!(
                "Device {:?} does not support required extensions {:?}",
                device_name, required_extensions_names
            );

            return 0;
        }

        let surface_details = Self::get_surface_details(surface_loader, device, surface).unwrap();

        if surface_details.formats.is_empty() {
            warn!(
                "Device {:?} does not support any usable surface formats",
                device_name
            );
            return 0;
        }

        if surface_details.present_modes.is_empty() {
            warn!(
                "Device {:?} does not support any usable present modes",
                device_name
            );
            return 0;
        }

        if device_properties.device_type == vk::PhysicalDeviceType::DISCRETE_GPU {
            score += 1000;
        } else if device_properties.device_type == vk::PhysicalDeviceType::INTEGRATED_GPU {
            score += 100;
        }

        score += device_properties.limits.max_image_dimension2_d;

        debug!("Score for {}: {}", device_name, score);

        score
    }

    unsafe fn get_surface_details(
        surface_loader: &surface::Instance,
        physical_device: vk::PhysicalDevice,
        surface: vk::SurfaceKHR,
    ) -> Result<SurfaceDetails> {
        let surface_capabilities = surface_loader
            .get_physical_device_surface_capabilities(physical_device, surface)
            .map_err(|e| anyhow!("{}", e))?;

        let surface_formats = surface_loader
            .get_physical_device_surface_formats(physical_device, surface)
            .map_err(|e| anyhow!("{}", e))?;

        let present_modes = surface_loader
            .get_physical_device_surface_present_modes(physical_device, surface)
            .map_err(|e| anyhow!("{}", e))?;

        Ok(SurfaceDetails {
            capabilities: surface_capabilities,
            formats: surface_formats,
            present_modes,
        })
    }

    pub unsafe fn pick_physical_device(
        instance: &Instance,
        required_extensions: &Vec<&CStr>,
        surface_loader: &surface::Instance,
        surface: vk::SurfaceKHR,
    ) -> Result<vk::PhysicalDevice> {
        let physical_devices = instance.enumerate_physical_devices()?;

        let scored_devices = physical_devices.iter().map(|&dev| {
            (
                Self::rate_device_suitability(
                    instance,
                    dev,
                    required_extensions,
                    surface_loader,
                    surface,
                ),
                dev,
            )
        });

        let mut suitable_device_map = BTreeMap::from_iter(scored_devices);

        let physical_device = suitable_device_map
            .last_entry()
            .map(|entry| {
                if *entry.key() > 0 {
                    Ok(*entry.get())
                } else {
                    Err(anyhow!("Could not get suitable physical device"))
                }
            })
            .unwrap_or(Err(anyhow!("No physical device found")))?;

        let device_properties = instance.get_physical_device_properties(physical_device);

        let device_name = CStr::from_ptr(device_properties.device_name.as_ptr()).to_string_lossy();

        debug!("Picked physical device {}", device_name);

        Ok(physical_device)
    }

    unsafe fn find_queue_family<P>(
        instance: &Instance,
        physical_device: vk::PhysicalDevice,
        predicate: P,
    ) -> Option<u32>
    where
        P: Fn(&usize, &vk::QueueFamilyProperties) -> bool,
    {
        let queue_family_properties =
            instance.get_physical_device_queue_family_properties(physical_device);

        queue_family_properties
            .iter()
            .enumerate()
            .find(|(i, qfp)| predicate(i, qfp))
            .map(|(i, _)| i as u32)
    }

    unsafe fn find_graphics_queue_family(
        instance: &Instance,
        physical_device: vk::PhysicalDevice,
    ) -> Option<u32> {
        Self::find_queue_family(instance, physical_device, |_, &qfp| {
            qfp.queue_flags.contains(vk::QueueFlags::GRAPHICS)
        })
    }

    unsafe fn find_present_queue_family(
        instance: &Instance,
        physical_device: vk::PhysicalDevice,
        surface_loader: &surface::Instance,
        surface: vk::SurfaceKHR,
    ) -> Option<u32> {
        Self::find_queue_family(instance, physical_device, |i, _| {
            surface_loader
                .get_physical_device_surface_support(physical_device, *i as u32, surface)
                .is_ok()
        })
    }

    unsafe fn find_transfer_queue_family(
        instance: &Instance,
        physical_device: vk::PhysicalDevice,
    ) -> Option<u32> {
        Self::find_queue_family(instance, physical_device, |_, &qfp| {
            qfp.queue_flags & (vk::QueueFlags::TRANSFER | vk::QueueFlags::GRAPHICS)
                == vk::QueueFlags::TRANSFER
        })
    }

    unsafe fn create_logical_device(
        instance: &Instance,
        device: vk::PhysicalDevice,
        queue_families: &QueueFamilies,
        required_extensions: &Vec<&CStr>,
    ) -> Result<Device> {
        let queue_create_infos = &[
            queue_families.graphics_family,
            queue_families.present_family,
            queue_families.transfer_family,
        ]
        .iter()
        .collect::<HashSet<_>>()
        .iter()
        .map(|&&qfi| {
            vk::DeviceQueueCreateInfo::default()
                .queue_family_index(qfi)
                .queue_priorities(&[1.0])
        })
        .collect::<Vec<_>>();

        let device_features = vk::PhysicalDeviceFeatures::default();

        let extensions = required_extensions
            .iter()
            .map(|&ext| ext.as_ptr())
            .collect::<Vec<_>>();

        let device_create_info = vk::DeviceCreateInfo::default()
            .queue_create_infos(&queue_create_infos)
            .enabled_features(&device_features)
            .enabled_extension_names(&extensions);

        instance
            .create_device(device, &device_create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    fn choose_surface_format(formats: &Vec<vk::SurfaceFormatKHR>) -> Result<vk::SurfaceFormatKHR> {
        let format = formats.iter().find(|&format| {
            format.format == vk::Format::B8G8R8A8_SRGB
                && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
        });

        if format.is_none() {
            return Err(anyhow!("No suitable surface format found"));
        }

        Ok(*format.unwrap())
    }

    fn choose_presentation_mode(
        present_modes: &Vec<vk::PresentModeKHR>,
    ) -> Result<vk::PresentModeKHR> {
        let present_mode = present_modes
            .iter()
            .find(|&&pm| pm == vk::PresentModeKHR::MAILBOX)
            .unwrap_or(&vk::PresentModeKHR::FIFO);

        debug!("Presentation mode is {:?}", present_mode);

        Ok(*present_mode)
    }

    fn choose_swap_extent(
        surface_details: &SurfaceDetails,
        window_size: PhysicalSize<u32>,
    ) -> vk::Extent2D {
        let extent = match surface_details.capabilities.current_extent.width {
            u32::MAX => vk::Extent2D {
                width: window_size.width,
                height: window_size.height,
            },
            _ => surface_details.capabilities.current_extent,
        };

        vk::Extent2D {
            width: extent.width.clamp(
                surface_details.capabilities.min_image_extent.width,
                surface_details.capabilities.max_image_extent.width,
            ),
            height: extent.height.clamp(
                surface_details.capabilities.min_image_extent.height,
                surface_details.capabilities.max_image_extent.height,
            ),
        }
    }

    unsafe fn create_swapchain(
        swapchain_loader: &swapchain::Device,
        surface_details: &SurfaceDetails,
        surface: vk::SurfaceKHR,
        format: vk::SurfaceFormatKHR,
        extent: vk::Extent2D,
        queue_families: &QueueFamilies,
    ) -> Result<vk::SwapchainKHR> {
        let present_mode = Self::choose_presentation_mode(&surface_details.present_modes)?;

        let mut image_count = surface_details.capabilities.min_image_count + 1;

        image_count = match surface_details.capabilities.max_image_count {
            0 => image_count,
            i => i.min(image_count),
        };

        let mut swapchain_create_info = vk::SwapchainCreateInfoKHR::default()
            .surface(surface)
            .min_image_count(image_count)
            .image_color_space(format.color_space)
            .image_format(format.format)
            .image_extent(extent)
            .present_mode(present_mode)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT);

        let queue_family_indices = &[
            queue_families.graphics_family,
            queue_families.present_family,
        ];

        if queue_families.graphics_family != queue_families.present_family {
            swapchain_create_info = swapchain_create_info
                .image_sharing_mode(vk::SharingMode::CONCURRENT)
                .queue_family_indices(queue_family_indices);
        } else {
            swapchain_create_info =
                swapchain_create_info.image_sharing_mode(vk::SharingMode::EXCLUSIVE);
        }

        swapchain_create_info = swapchain_create_info
            .pre_transform(surface_details.capabilities.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .clipped(true);

        swapchain_loader
            .create_swapchain(&swapchain_create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    unsafe fn create_image_views(
        images: &Vec<vk::Image>,
        format: &vk::SurfaceFormatKHR,
        device: &Device,
    ) -> Result<Vec<vk::ImageView>> {
        Ok(images
            .iter()
            .map(|&img| Self::create_image_view(img, format.format, device).unwrap())
            .collect::<Vec<_>>())
    }

    unsafe fn create_image_view(
        image: vk::Image,
        format: vk::Format,
        device: &Device,
    ) -> Result<vk::ImageView> {
        let create_info = vk::ImageViewCreateInfo::default()
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(format)
            .components(vk::ComponentMapping {
                r: vk::ComponentSwizzle::IDENTITY,
                g: vk::ComponentSwizzle::IDENTITY,
                b: vk::ComponentSwizzle::IDENTITY,
                a: vk::ComponentSwizzle::IDENTITY,
            })
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });

        device
            .create_image_view(&create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    unsafe fn create_shader_module(&self, shader_path: &str) -> Result<vk::ShaderModule> {
        let result = std::fs::read(shader_path)?;

        let mut shader_spv = Cursor::new(result);

        let shader_code = read_spv(&mut shader_spv)?;

        let create_info = vk::ShaderModuleCreateInfo::default().code(&shader_code);

        self.device
            .create_shader_module(&create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    unsafe fn create_pipeline_stage<'a>(
        shader_module: vk::ShaderModule,
        shader_stage: vk::ShaderStageFlags,
    ) -> vk::PipelineShaderStageCreateInfo<'a> {
        vk::PipelineShaderStageCreateInfo::default()
            .stage(shader_stage)
            .module(shader_module)
            .name(CStr::from_bytes_with_nul_unchecked(b"main\0"))
    }

    pub fn create_render_pass(
        &self,
        attachments: &[vk::AttachmentDescription],
        subpasses: &[vk::SubpassDescription],
        dependencies: &[vk::SubpassDependency],
    ) -> Result<vk::RenderPass> {
        let attachments = attachments
            .iter()
            .map(|at| at.format(self.swapchain_details.format.format))
            .collect::<Vec<_>>();

        let create_info = vk::RenderPassCreateInfo::default()
            .attachments(&attachments)
            .subpasses(subpasses)
            .dependencies(dependencies);

        unsafe {
            self.device
                .create_render_pass(&create_info, None)
                .map_err(|e| anyhow!("{}", e))
        }
    }

    pub fn create_pipeline_layout(
        &self,
        create_info: &vk::PipelineLayoutCreateInfo,
    ) -> Result<vk::PipelineLayout> {
        unsafe {
            self.device
                .create_pipeline_layout(&create_info, None)
                .map_err(|e| anyhow!("{}", e))
        }
    }

    pub fn create_graphics_pipeline(
        &self,
        vertex_shader: &str,
        fragment_shader: &str,
        vertex_input_state: &vk::PipelineVertexInputStateCreateInfo,
        input_assembly_state: &vk::PipelineInputAssemblyStateCreateInfo,
        rasterization_state: &vk::PipelineRasterizationStateCreateInfo,
        multisample_state: &vk::PipelineMultisampleStateCreateInfo,
        depth_stencil_state: Option<&vk::PipelineDepthStencilStateCreateInfo>,
        color_blend_state: &vk::PipelineColorBlendStateCreateInfo,
        dynamic_state: &vk::PipelineDynamicStateCreateInfo,
        pipeline_layout: vk::PipelineLayout,
        render_pass: vk::RenderPass,
        subpass_index: u32,
    ) -> Result<vk::Pipeline> {
        let vertex_shader_module = unsafe { self.create_shader_module(vertex_shader)? };
        let fragment_shader_module = unsafe { self.create_shader_module(fragment_shader)? };

        let vertex_stage = unsafe {
            Self::create_pipeline_stage(vertex_shader_module, vk::ShaderStageFlags::VERTEX)
        };

        let fragment_stage = unsafe {
            Self::create_pipeline_stage(fragment_shader_module, vk::ShaderStageFlags::FRAGMENT)
        };

        let viewport_state = vk::PipelineViewportStateCreateInfo::default()
            .scissor_count(1)
            .viewport_count(1);

        let stages = &[vertex_stage, fragment_stage];

        let mut pipeline_create_info = vk::GraphicsPipelineCreateInfo::default()
            .stages(stages)
            .vertex_input_state(vertex_input_state)
            .input_assembly_state(input_assembly_state)
            .viewport_state(&viewport_state)
            .rasterization_state(rasterization_state)
            .multisample_state(multisample_state);

        if depth_stencil_state.is_some() {
            pipeline_create_info =
                pipeline_create_info.depth_stencil_state(depth_stencil_state.unwrap());
        }

        pipeline_create_info = pipeline_create_info
            .color_blend_state(color_blend_state)
            .dynamic_state(dynamic_state)
            .layout(pipeline_layout)
            .render_pass(render_pass)
            .subpass(subpass_index);

        let pipelines = unsafe {
            self.device
                .create_graphics_pipelines(vk::PipelineCache::null(), &[pipeline_create_info], None)
                .map_err(|e| anyhow!("{}", e.1))?
        };

        unsafe {
            self.device
                .destroy_shader_module(vertex_shader_module, None);
            self.device
                .destroy_shader_module(fragment_shader_module, None);
        }

        Ok(*pipelines.first().expect("Error creating pipeline"))
    }

    pub fn create_swapchain_framebuffers(
        &self,
        render_pass: vk::RenderPass,
    ) -> Vec<vk::Framebuffer> {
        self.swapchain_details
            .image_views
            .iter()
            .map(|&iv| {
                let attachments = &[iv];

                let create_info = vk::FramebufferCreateInfo::default()
                    .render_pass(render_pass)
                    .attachment_count(1)
                    .attachments(attachments)
                    .width(self.swapchain_details.extent.width)
                    .height(self.swapchain_details.extent.height)
                    .layers(1);

                unsafe { self.device.create_framebuffer(&create_info, None).unwrap() }
            })
            .collect::<Vec<_>>()
    }

    pub fn create_command_pool(
        &self,
        graphics_family: u32,
        flags: vk::CommandPoolCreateFlags,
    ) -> Result<vk::CommandPool> {
        let create_info = vk::CommandPoolCreateInfo::default()
            .queue_family_index(graphics_family)
            .flags(flags);

        unsafe {
            self.device
                .create_command_pool(&create_info, None)
                .map_err(|e| anyhow!("{}", e))
        }
    }

    pub fn create_graphics_command_buffers(
        &self,
        command_pool: vk::CommandPool,
        buffer_count: u32,
        command_buffer_level: vk::CommandBufferLevel,
    ) -> Result<Vec<vk::CommandBuffer>> {
        let create_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(command_pool)
            .level(command_buffer_level)
            .command_buffer_count(buffer_count);

        unsafe {
            self.device
                .allocate_command_buffers(&create_info)
                .map_err(|e| anyhow!("{}", e))
        }
    }

    pub fn recreate_swapchain(
        &mut self,
        window: &Window,
        framebuffers: &Vec<vk::Framebuffer>,
        render_pass: vk::RenderPass,
    ) -> Result<Vec<vk::Framebuffer>> {
        warn!("Re-creating the swapchain");

        unsafe { self.device.device_wait_idle()? };

        framebuffers
            .iter()
            .for_each(|&fb| unsafe { self.device.destroy_framebuffer(fb, None) });

        unsafe { self.cleanup_swapchain() };

        let surface_details = unsafe {
            Self::get_surface_details(&self.surface_loader, self.physical_device, self.surface)
        }?;

        let format = Self::choose_surface_format(&surface_details.formats)?;

        let extent = Self::choose_swap_extent(&surface_details, window.inner_size());

        self.surface_details = surface_details;
        self.swapchain_details.format = format;
        self.swapchain_details.extent = extent;

        let swapchain = unsafe {
            Self::create_swapchain(
                &self.swapchain_loader,
                &self.surface_details,
                self.surface,
                self.swapchain_details.format,
                self.swapchain_details.extent,
                &self.queue_families,
            )?
        };

        self.swapchain = swapchain;

        let images = unsafe { self.swapchain_loader.get_swapchain_images(swapchain)? };

        let image_views = unsafe { Self::create_image_views(&images, &format, &self.device) }?;

        self.images = images;
        self.swapchain_details.image_views = image_views;

        Ok(self.create_swapchain_framebuffers(render_pass))
    }

    unsafe fn cleanup_swapchain(&self) {
        self.swapchain_details
            .image_views
            .iter()
            .for_each(|&iv| self.device.destroy_image_view(iv, None));

        self.swapchain_loader
            .destroy_swapchain(self.swapchain, None);
    }

    pub unsafe fn create_buffer(
        &self,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
        queue_families: &[u32],
    ) -> Result<vk::Buffer> {
        let mut create_info = vk::BufferCreateInfo::default()
            .usage(usage)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .size(size);

        let unique_queue_families = queue_families.iter().map(|&it| it).collect::<HashSet<_>>();

        if unique_queue_families.len() > 1 {
            create_info = create_info
                .sharing_mode(vk::SharingMode::CONCURRENT)
                .queue_family_indices(queue_families);
        }

        self.device
            .create_buffer(&create_info, None)
            .map_err(|e| anyhow!("{}", e))
    }

    pub unsafe fn copy_buffer(
        &self,
        command_pool: vk::CommandPool,
        src: vk::Buffer,
        src_offset: u64,
        dst: vk::Buffer,
        dst_offset: u64,
        size: vk::DeviceSize,
    ) -> Result<()> {
        let alloc_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(command_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = *self
            .device
            .allocate_command_buffers(&alloc_info)?
            .first()
            .unwrap();

        let begin_info = vk::CommandBufferBeginInfo::default()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        self.device
            .begin_command_buffer(command_buffer, &begin_info)?;

        let copy_region = vk::BufferCopy::default()
            .size(size)
            .src_offset(src_offset)
            .dst_offset(dst_offset);

        let copy_regions = &[copy_region];

        self.device
            .cmd_copy_buffer(command_buffer, src, dst, copy_regions);

        self.device.end_command_buffer(command_buffer)?;

        let commands_buffers = &[command_buffer];

        let submit_info = vk::SubmitInfo::default().command_buffers(commands_buffers);

        self.device
            .queue_submit(self.transfer_queue, &[submit_info], vk::Fence::null())?;

        self.device
            .queue_wait_idle(self.transfer_queue)
            .map_err(|e| anyhow!("{}", e))?;

        self.device
            .free_command_buffers(command_pool, commands_buffers);

        Ok(())
    }

    pub fn alignment_padding(size: usize, alignment: vk::DeviceSize) -> vk::DeviceSize {
        if size as u64 % alignment == 0 {
            0
        } else {
            alignment - (size as u64 % alignment)
        }
    }

    pub unsafe fn copy_data_to_memory<T: Copy>(
        &self,
        data: &Vec<T>,
        memory: vk::DeviceMemory,
        memory_offset: vk::DeviceSize,
    ) -> Result<()> {
        let size = mem::size_of::<T>() * data.len();

        let buffer_ptr = unsafe {
            self.device.map_memory(
                memory,
                memory_offset,
                size as u64,
                vk::MemoryMapFlags::empty(),
            )?
        };

        let mut buffer_align =
            unsafe { Align::new(buffer_ptr, mem::align_of::<T>() as u64, size as u64) };

        buffer_align.copy_from_slice(data);

        unsafe {
            self.device.unmap_memory(memory);
        }

        Ok(())
    }

    pub unsafe fn allocate_memory(
        &self,
        buffers: &Vec<vk::Buffer>,
        memory_flags: vk::MemoryPropertyFlags,
    ) -> Result<AllocationInfo> {
        let memory_requirements = buffers
            .iter()
            .map(|&buf| self.device.get_buffer_memory_requirements(buf))
            .collect::<Vec<_>>();

        // Use maximum alignment across all buffers (safety over optimization)
        let memory_alignment = memory_requirements
            .iter()
            .map(|reqs| reqs.alignment)
            .fold(0, |al_max, al| al_max.max(al));

        // Allocate blocks of size `memory_alignment`
        let memory_size = memory_requirements
            .iter()
            .map(|reqs| reqs.size)
            .collect::<Vec<_>>()
            .iter()
            .fold(0 as vk::DeviceSize, |sz_sum, sz| {
                sz_sum + memory_alignment * sz.div_ceil(memory_alignment)
            });

        debug!(
            "Memory size: {}, alignment: {}",
            memory_size, memory_alignment
        );

        let memory_properties = self
            .instance
            .get_physical_device_memory_properties(self.physical_device);

        let memory_type_index = memory_properties
            .memory_types
            .iter()
            .enumerate()
            .find(|&(index, memory_type)| {
                let is_suitable = Self::suitable_memory_type(
                    index,
                    memory_type,
                    &memory_requirements,
                    memory_flags,
                );
                if is_suitable {
                    debug!("Found suitable {:?}", memory_type);
                }
                is_suitable
            })
            .ok_or(anyhow!("Could not find suitable memory type for buffers"))
            .map(|(i, _)| i as u32)?;

        let allocate_info = vk::MemoryAllocateInfo::default()
            .memory_type_index(memory_type_index)
            .allocation_size(memory_size);

        self.device
            .allocate_memory(&allocate_info, None)
            .map(|mem| AllocationInfo {
                memory: mem,
                alignment: memory_alignment,
                size: memory_size,
            })
            .map_err(|e| anyhow!("{}", e))
    }

    fn suitable_memory_type(
        memory_index: usize,
        memory_type: &vk::MemoryType,
        memory_requirements: &[vk::MemoryRequirements],
        memory_flags: vk::MemoryPropertyFlags,
    ) -> bool {
        memory_requirements
            .iter()
            .all(|reqs| reqs.memory_type_bits & (1 << memory_index) > 0)
            && memory_type.property_flags & memory_flags == memory_flags
    }

    pub unsafe fn acquire_next_image(
        &mut self,
        semaphore_to_signal: vk::Semaphore,
        fence_to_signal: vk::Fence,
        window: &Window,
        framebuffers: &Vec<vk::Framebuffer>,
        render_pass: vk::RenderPass,
    ) -> Result<(Option<u32>, Option<Vec<vk::Framebuffer>>)> {
        let next_image = self.swapchain_loader.acquire_next_image(
            self.swapchain,
            u64::MAX,
            semaphore_to_signal,
            fence_to_signal,
        );

        return if let Ok((idx, suboptimal)) = next_image {
            if suboptimal {
                warn!("Swapchain is suboptimal");

                let framebuffers = self.recreate_swapchain(window, framebuffers, render_pass)?;

                return Ok((None, Some(framebuffers)));
            }

            Ok((Some(idx), None))
        } else if let Err(vk::Result::ERROR_OUT_OF_DATE_KHR) = next_image {
            warn!("Swapchain is out of date");

            let framebuffers = self.recreate_swapchain(
                window,
                framebuffers,
                render_pass,
            )?;

            Ok((None, Some(framebuffers)))
        } else {
            Err(anyhow!(
                "Failed to acquire next image from the swapchain: {}",
                next_image.unwrap_err()
            ))
        }
    }
}

unsafe extern "system" fn debug_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT<'_>,
    _user_data: *mut std::os::raw::c_void,
) -> vk::Bool32 {
    let data = *p_callback_data;

    let message = CStr::from_ptr(data.p_message).to_string_lossy();

    if message_severity >= vk::DebugUtilsMessageSeverityFlagsEXT::ERROR {
        error!("({:?}) {}", message_type, message);
    } else if message_severity >= vk::DebugUtilsMessageSeverityFlagsEXT::WARNING {
        warn!("({:?}) {}", message_type, message);
    } else if message_severity >= vk::DebugUtilsMessageSeverityFlagsEXT::INFO {
        debug!("({:?}) {}", message_type, message);
    } else {
        trace!("({:?}) {}", message_type, message);
    }

    vk::FALSE
}

impl Drop for Sulfur {
    fn drop(&mut self) {
        unsafe {
            self.cleanup_swapchain();
            self.device.destroy_device(None);
            if self.messenger.is_some() {
                self.debug_utils_loader
                    .destroy_debug_utils_messenger(self.messenger.unwrap(), None);
            }
            self.surface_loader.destroy_surface(self.surface, None);
            self.instance.destroy_instance(None);
        };
    }
}
