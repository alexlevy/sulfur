use std::mem;

use ash::vk;

#[derive(Clone, Debug, Copy)]
pub struct Vertex {
    pub position: [f32; 2],
    pub color: [f32; 3],
}

impl Vertex {
    pub fn get_binding_description(binding: u32) -> vk::VertexInputBindingDescription {
        vk::VertexInputBindingDescription::default()
            .binding(binding)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
    }

    pub fn get_attribute_description(
        binding: u32,
        location: u32,
        offset: usize,
        format: vk::Format,
    ) -> vk::VertexInputAttributeDescription {
        vk::VertexInputAttributeDescription::default()
            .format(format)
            .location(location)
            .binding(binding)
            .offset(offset as u32)
    }
}
