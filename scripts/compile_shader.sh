#!/bin/bash

# shellcheck disable=SC2046
set -- $(getopt -u -o hcd:n:k: --long --help,--clean,shader-dir:,shader-name:,vulkan-dir: -- "$@")

shader_dir=$(dirname "$0")
shader_name=
vulkan_bin_dir=$(dirname "$(which glslc 2>/dev/null)" 2>/dev/null)
err=0

help() {
  echo "Usage:"
  printf "\t%s\n" "$0 [-h] [-c|--clean] [-k|--vulkan-bin-dir VULKAN_BIN_DIR] [-d|--shader-dir SHADER_DIR] -n|--shader-name SHADER_NAME"
  echo "Options:"
  printf "\t%-20s %s\n" "-h|--help" "Show this help"
  printf "\t%-20s %s\n" "-c|--clean" "Delete build directory and its contents"
  printf "\t%-20s %s\n" "-d|--shader-dir" "Specify the directory of shaders to compile (default: script's directory)"
  printf "\t%-20s %s\n" "-n|--shader-name" "Specify the base name of shaders to compile"
  printf "\t%-20s %s\n" "-k|--vulkan-bin-dir" "Specify Vulkan SDK bin directory (not necessary if it is in PATH)"
}

clean() {
  if [[ -d "${shader_dir}/build" ]]; then
    rm -rf "${shader_dir}"/build
  else
    echo -e "\e[1;33m[WARN]\e[0m ${shader_dir}/build is not a directory. Nothing was cleaned"
  fi
}

while :
do
  case "$1" in
    -h|--help) help ; shift ; exit 0 ;;
    -c|--clean) clean ; shift ; exit 0;;
    -d|--shader-dir) shader_dir=$2 ; shift 2 ;;
    -n|--shader-name) shader_name=$2 ; shift 2 ;;
    -k|--vulkan-dir) vulkan_bin_dir=$2 ; shift 2 ;;
    --) shift ; break ;;
    *) break;;
  esac
done

if [[ ! -d "${shader_dir}" ]]; then
  echo -e "\e[1;31m[ERR]\e[0m ${shader_dir} is not a directory"
  err=1
fi

if [[ -z "${shader_name}" ]]; then
  echo -e "\e[1;31m[ERR]\e[0m -n|--shader-name is mandatory"
  err=1
fi

if [[ -z "${vulkan_bin_dir}" ]]; then
  echo -e "\e[1;31m[ERR]\e[0m Could not find vulkan SDK directory. Please specify -k|--vulkan-dir option"
  err=1
fi

if [[ ! -f "${vulkan_bin_dir}/glslc" ]]; then
  echo -e "\e[1;31m[ERR]\e[0m SPIR-V compiler \e[4mglslc\e[base namebase name0m was not found in ${vulkan_bin_dir}"
  err=1
fi

vertex_shader="${shader_name}.vert"
fragment_shader="${shader_name}.frag"

for shader in ${shader_dir}/${vertex_shader} ${shader_dir}/${fragment_shader}
do
  if [[ ! -f "${shader}" ]] ; then
    echo -e "\e[1;31m[ERR]\e[0m ${shader} was not found"
    err=1
  fi
done

[[ $err -gt 0 ]] && exit 1

[[ ! -d "${shader_dir}/build" ]] && mkdir -p "${shader_dir}"/build

for shader in ${vertex_shader} ${fragment_shader}
do
  "${vulkan_bin_dir}"/glslc "${shader_dir}/${shader}" -o "${shader_dir}/build/${shader/./_}".spv
done